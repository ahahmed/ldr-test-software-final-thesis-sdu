Timestamp GlobalTime(s) Dose(Mrad)
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'TEMPSENS_1': 961}
34 {'TEMPSENS_1': 958}
36 {'TEMPSENS_1': 954}
38 {'TEMPSENS_1': 959}
40 {'TEMPSENS_1': 955}
42 {'TEMPSENS_1': 955}
44 {'TEMPSENS_1': 959}
46 {'TEMPSENS_1': 953}
48 {'TEMPSENS_1': 954}
50 {'TEMPSENS_1': 952}
52 {'TEMPSENS_1': 960}
54 {'TEMPSENS_1': 951}
56 {'TEMPSENS_1': 956}
58 {'TEMPSENS_1': 948}
60 {'TEMPSENS_1': 954}
62 {'TEMPSENS_1': 957}
33 {'TEMPSENS_1': 1395}
35 {'TEMPSENS_1': 1395}
37 {'TEMPSENS_1': 1396}
39 {'TEMPSENS_1': 1395}
41 {'TEMPSENS_1': 1395}
43 {'TEMPSENS_1': 1396}
45 {'TEMPSENS_1': 1396}
47 {'TEMPSENS_1': 1396}
49 {'TEMPSENS_1': 1396}
51 {'TEMPSENS_1': 1397}
53 {'TEMPSENS_1': 1396}
55 {'TEMPSENS_1': 1396}
57 {'TEMPSENS_1': 1396}
59 {'TEMPSENS_1': 1397}
61 {'TEMPSENS_1': 1396}
63 {'TEMPSENS_1': 1395}
32 {'TEMPSENS_2': 942}
34 {'TEMPSENS_2': 935}
36 {'TEMPSENS_2': 932}
38 {'TEMPSENS_2': 939}
40 {'TEMPSENS_2': 930}
42 {'TEMPSENS_2': 936}
44 {'TEMPSENS_2': 933}
46 {'TEMPSENS_2': 939}
48 {'TEMPSENS_2': 932}
50 {'TEMPSENS_2': 936}
52 {'TEMPSENS_2': 937}
54 {'TEMPSENS_2': 940}
56 {'TEMPSENS_2': 933}
58 {'TEMPSENS_2': 938}
60 {'TEMPSENS_2': 935}
62 {'TEMPSENS_2': 937}
33 {'TEMPSENS_2': 1377}
35 {'TEMPSENS_2': 1378}
37 {'TEMPSENS_2': 1378}
39 {'TEMPSENS_2': 1378}
41 {'TEMPSENS_2': 1378}
43 {'TEMPSENS_2': 1378}
45 {'TEMPSENS_2': 1378}
47 {'TEMPSENS_2': 1377}
49 {'TEMPSENS_2': 1377}
51 {'TEMPSENS_2': 1377}
53 {'TEMPSENS_2': 1377}
55 {'TEMPSENS_2': 1377}
57 {'TEMPSENS_2': 1378}
59 {'TEMPSENS_2': 1378}
61 {'TEMPSENS_2': 1377}
63 {'TEMPSENS_2': 1378}
32 {'TEMPSENS_3': 919}
34 {'TEMPSENS_3': 917}
36 {'TEMPSENS_3': 915}
38 {'TEMPSENS_3': 915}
40 {'TEMPSENS_3': 918}
42 {'TEMPSENS_3': 915}
44 {'TEMPSENS_3': 913}
46 {'TEMPSENS_3': 915}
48 {'TEMPSENS_3': 914}
50 {'TEMPSENS_3': 919}
52 {'TEMPSENS_3': 920}
54 {'TEMPSENS_3': 916}
56 {'TEMPSENS_3': 915}
58 {'TEMPSENS_3': 914}
60 {'TEMPSENS_3': 915}
62 {'TEMPSENS_3': 916}
33 {'TEMPSENS_3': 1359}
35 {'TEMPSENS_3': 1360}
37 {'TEMPSENS_3': 1360}
39 {'TEMPSENS_3': 1360}
41 {'TEMPSENS_3': 1360}
43 {'TEMPSENS_3': 1360}
45 {'TEMPSENS_3': 1360}
47 {'TEMPSENS_3': 1360}
49 {'TEMPSENS_3': 1360}
51 {'TEMPSENS_3': 1359}
53 {'TEMPSENS_3': 1359}
55 {'TEMPSENS_3': 1359}
57 {'TEMPSENS_3': 1360}
59 {'TEMPSENS_3': 1359}
61 {'TEMPSENS_3': 1359}
63 {'TEMPSENS_3': 1361}
32 {'TEMPSENS_4': 917}
34 {'TEMPSENS_4': 913}
36 {'TEMPSENS_4': 919}
38 {'TEMPSENS_4': 918}
40 {'TEMPSENS_4': 920}
42 {'TEMPSENS_4': 919}
44 {'TEMPSENS_4': 914}
46 {'TEMPSENS_4': 917}
48 {'TEMPSENS_4': 915}
50 {'TEMPSENS_4': 919}
52 {'TEMPSENS_4': 912}
54 {'TEMPSENS_4': 915}
56 {'TEMPSENS_4': 917}
58 {'TEMPSENS_4': 918}
60 {'TEMPSENS_4': 918}
62 {'TEMPSENS_4': 914}
33 {'TEMPSENS_4': 1360}
35 {'TEMPSENS_4': 1360}
37 {'TEMPSENS_4': 1359}
39 {'TEMPSENS_4': 1359}
41 {'TEMPSENS_4': 1359}
43 {'TEMPSENS_4': 1359}
45 {'TEMPSENS_4': 1359}
47 {'TEMPSENS_4': 1360}
49 {'TEMPSENS_4': 1360}
51 {'TEMPSENS_4': 1360}
53 {'TEMPSENS_4': 1360}
55 {'TEMPSENS_4': 1360}
57 {'TEMPSENS_4': 1360}
59 {'TEMPSENS_4': 1359}
61 {'TEMPSENS_4': 1359}
63 {'TEMPSENS_4': 1359}
32 {'RADSENS_1': 2563}
34 {'RADSENS_1': 2572}
36 {'RADSENS_1': 2569}
38 {'RADSENS_1': 2568}
40 {'RADSENS_1': 2568}
42 {'RADSENS_1': 2567}
44 {'RADSENS_1': 2562}
46 {'RADSENS_1': 2566}
48 {'RADSENS_1': 2569}
50 {'RADSENS_1': 2567}
52 {'RADSENS_1': 2569}
54 {'RADSENS_1': 2565}
56 {'RADSENS_1': 2567}
58 {'RADSENS_1': 2569}
60 {'RADSENS_1': 2569}
62 {'RADSENS_1': 2574}
33 {'RADSENS_1': 2921}
35 {'RADSENS_1': 2920}
37 {'RADSENS_1': 2922}
39 {'RADSENS_1': 2921}
41 {'RADSENS_1': 2922}
43 {'RADSENS_1': 2923}
45 {'RADSENS_1': 2922}
47 {'RADSENS_1': 2921}
49 {'RADSENS_1': 2922}
51 {'RADSENS_1': 2922}
53 {'RADSENS_1': 2921}
55 {'RADSENS_1': 2921}
57 {'RADSENS_1': 2922}
59 {'RADSENS_1': 2921}
61 {'RADSENS_1': 2922}
63 {'RADSENS_1': 2921}
32 {'RADSENS_2': 2548}
34 {'RADSENS_2': 2549}
36 {'RADSENS_2': 2551}
38 {'RADSENS_2': 2554}
40 {'RADSENS_2': 2556}
42 {'RADSENS_2': 2549}
44 {'RADSENS_2': 2551}
46 {'RADSENS_2': 2554}
48 {'RADSENS_2': 2552}
50 {'RADSENS_2': 2556}
52 {'RADSENS_2': 2552}
54 {'RADSENS_2': 2548}
56 {'RADSENS_2': 2554}
58 {'RADSENS_2': 2554}
60 {'RADSENS_2': 2550}
62 {'RADSENS_2': 2551}
33 {'RADSENS_2': 2906}
35 {'RADSENS_2': 2906}
37 {'RADSENS_2': 2908}
39 {'RADSENS_2': 2907}
41 {'RADSENS_2': 2905}
43 {'RADSENS_2': 2907}
45 {'RADSENS_2': 2906}
47 {'RADSENS_2': 2907}
49 {'RADSENS_2': 2906}
51 {'RADSENS_2': 2906}
53 {'RADSENS_2': 2906}
55 {'RADSENS_2': 2907}
57 {'RADSENS_2': 2907}
59 {'RADSENS_2': 2906}
61 {'RADSENS_2': 2906}
63 {'RADSENS_2': 2906}
32 {'RADSENS_3': 2534}
34 {'RADSENS_3': 2533}
36 {'RADSENS_3': 2535}
38 {'RADSENS_3': 2534}
40 {'RADSENS_3': 2531}
42 {'RADSENS_3': 2530}
44 {'RADSENS_3': 2532}
46 {'RADSENS_3': 2535}
48 {'RADSENS_3': 2532}
50 {'RADSENS_3': 2532}
52 {'RADSENS_3': 2535}
54 {'RADSENS_3': 2537}
56 {'RADSENS_3': 2531}
58 {'RADSENS_3': 2533}
60 {'RADSENS_3': 2536}
62 {'RADSENS_3': 2534}
33 {'RADSENS_3': 2891}
35 {'RADSENS_3': 2890}
37 {'RADSENS_3': 2889}
39 {'RADSENS_3': 2890}
41 {'RADSENS_3': 2890}
43 {'RADSENS_3': 2890}
45 {'RADSENS_3': 2890}
47 {'RADSENS_3': 2889}
49 {'RADSENS_3': 2891}
51 {'RADSENS_3': 2890}
53 {'RADSENS_3': 2891}
55 {'RADSENS_3': 2889}
57 {'RADSENS_3': 2890}
59 {'RADSENS_3': 2890}
61 {'RADSENS_3': 2890}
63 {'RADSENS_3': 2890}
32 {'RADSENS_4': 2526}
34 {'RADSENS_4': 2529}
36 {'RADSENS_4': 2533}
38 {'RADSENS_4': 2531}
40 {'RADSENS_4': 2526}
42 {'RADSENS_4': 2527}
44 {'RADSENS_4': 2525}
46 {'RADSENS_4': 2531}
48 {'RADSENS_4': 2529}
50 {'RADSENS_4': 2527}
52 {'RADSENS_4': 2529}
54 {'RADSENS_4': 2528}
56 {'RADSENS_4': 2525}
58 {'RADSENS_4': 2531}
60 {'RADSENS_4': 2527}
62 {'RADSENS_4': 2527}
33 {'RADSENS_4': 2885}
35 {'RADSENS_4': 2885}
37 {'RADSENS_4': 2884}
39 {'RADSENS_4': 2883}
41 {'RADSENS_4': 2884}
43 {'RADSENS_4': 2884}
45 {'RADSENS_4': 2884}
47 {'RADSENS_4': 2884}
49 {'RADSENS_4': 2884}
51 {'RADSENS_4': 2883}
53 {'RADSENS_4': 2884}
55 {'RADSENS_4': 2884}
57 {'RADSENS_4': 2884}
59 {'RADSENS_4': 2884}
61 {'RADSENS_4': 2885}
63 {'RADSENS_4': 2884}
