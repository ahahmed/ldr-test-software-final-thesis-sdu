Timestamp GlobalTime(s) Dose(Mrad)
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'TEMPSENS_1': 961}
34 {'TEMPSENS_1': 957}
36 {'TEMPSENS_1': 955}
38 {'TEMPSENS_1': 959}
40 {'TEMPSENS_1': 955}
42 {'TEMPSENS_1': 955}
44 {'TEMPSENS_1': 959}
46 {'TEMPSENS_1': 952}
48 {'TEMPSENS_1': 954}
50 {'TEMPSENS_1': 952}
52 {'TEMPSENS_1': 960}
54 {'TEMPSENS_1': 951}
56 {'TEMPSENS_1': 956}
58 {'TEMPSENS_1': 948}
60 {'TEMPSENS_1': 955}
62 {'TEMPSENS_1': 958}
33 {'TEMPSENS_1': 1395}
35 {'TEMPSENS_1': 1396}
37 {'TEMPSENS_1': 1395}
39 {'TEMPSENS_1': 1394}
41 {'TEMPSENS_1': 1396}
43 {'TEMPSENS_1': 1395}
45 {'TEMPSENS_1': 1396}
47 {'TEMPSENS_1': 1396}
49 {'TEMPSENS_1': 1395}
51 {'TEMPSENS_1': 1396}
53 {'TEMPSENS_1': 1395}
55 {'TEMPSENS_1': 1397}
57 {'TEMPSENS_1': 1396}
59 {'TEMPSENS_1': 1395}
61 {'TEMPSENS_1': 1395}
63 {'TEMPSENS_1': 1396}
32 {'TEMPSENS_2': 942}
34 {'TEMPSENS_2': 936}
36 {'TEMPSENS_2': 934}
38 {'TEMPSENS_2': 940}
40 {'TEMPSENS_2': 931}
42 {'TEMPSENS_2': 937}
44 {'TEMPSENS_2': 934}
46 {'TEMPSENS_2': 939}
48 {'TEMPSENS_2': 932}
50 {'TEMPSENS_2': 937}
52 {'TEMPSENS_2': 938}
54 {'TEMPSENS_2': 940}
56 {'TEMPSENS_2': 933}
58 {'TEMPSENS_2': 938}
60 {'TEMPSENS_2': 936}
62 {'TEMPSENS_2': 937}
33 {'TEMPSENS_2': 1377}
35 {'TEMPSENS_2': 1378}
37 {'TEMPSENS_2': 1378}
39 {'TEMPSENS_2': 1377}
41 {'TEMPSENS_2': 1378}
43 {'TEMPSENS_2': 1378}
45 {'TEMPSENS_2': 1378}
47 {'TEMPSENS_2': 1377}
49 {'TEMPSENS_2': 1378}
51 {'TEMPSENS_2': 1378}
53 {'TEMPSENS_2': 1377}
55 {'TEMPSENS_2': 1377}
57 {'TEMPSENS_2': 1378}
59 {'TEMPSENS_2': 1377}
61 {'TEMPSENS_2': 1378}
63 {'TEMPSENS_2': 1378}
32 {'TEMPSENS_3': 919}
34 {'TEMPSENS_3': 917}
36 {'TEMPSENS_3': 915}
38 {'TEMPSENS_3': 916}
40 {'TEMPSENS_3': 917}
42 {'TEMPSENS_3': 915}
44 {'TEMPSENS_3': 914}
46 {'TEMPSENS_3': 915}
48 {'TEMPSENS_3': 916}
50 {'TEMPSENS_3': 919}
52 {'TEMPSENS_3': 920}
54 {'TEMPSENS_3': 916}
56 {'TEMPSENS_3': 914}
58 {'TEMPSENS_3': 915}
60 {'TEMPSENS_3': 915}
62 {'TEMPSENS_3': 916}
33 {'TEMPSENS_3': 1360}
35 {'TEMPSENS_3': 1360}
37 {'TEMPSENS_3': 1360}
39 {'TEMPSENS_3': 1360}
41 {'TEMPSENS_3': 1360}
43 {'TEMPSENS_3': 1360}
45 {'TEMPSENS_3': 1360}
47 {'TEMPSENS_3': 1360}
49 {'TEMPSENS_3': 1360}
51 {'TEMPSENS_3': 1359}
53 {'TEMPSENS_3': 1359}
55 {'TEMPSENS_3': 1360}
57 {'TEMPSENS_3': 1360}
59 {'TEMPSENS_3': 1360}
61 {'TEMPSENS_3': 1360}
63 {'TEMPSENS_3': 1360}
32 {'TEMPSENS_4': 917}
34 {'TEMPSENS_4': 913}
36 {'TEMPSENS_4': 919}
38 {'TEMPSENS_4': 918}
40 {'TEMPSENS_4': 921}
42 {'TEMPSENS_4': 920}
44 {'TEMPSENS_4': 914}
46 {'TEMPSENS_4': 917}
48 {'TEMPSENS_4': 915}
50 {'TEMPSENS_4': 918}
52 {'TEMPSENS_4': 912}
54 {'TEMPSENS_4': 914}
56 {'TEMPSENS_4': 917}
58 {'TEMPSENS_4': 917}
60 {'TEMPSENS_4': 918}
62 {'TEMPSENS_4': 914}
33 {'TEMPSENS_4': 1360}
35 {'TEMPSENS_4': 1361}
37 {'TEMPSENS_4': 1360}
39 {'TEMPSENS_4': 1359}
41 {'TEMPSENS_4': 1358}
43 {'TEMPSENS_4': 1360}
45 {'TEMPSENS_4': 1360}
47 {'TEMPSENS_4': 1359}
49 {'TEMPSENS_4': 1359}
51 {'TEMPSENS_4': 1360}
53 {'TEMPSENS_4': 1359}
55 {'TEMPSENS_4': 1359}
57 {'TEMPSENS_4': 1360}
59 {'TEMPSENS_4': 1359}
61 {'TEMPSENS_4': 1359}
63 {'TEMPSENS_4': 1360}
32 {'RADSENS_1': 2565}
34 {'RADSENS_1': 2572}
36 {'RADSENS_1': 2571}
38 {'RADSENS_1': 2569}
40 {'RADSENS_1': 2568}
42 {'RADSENS_1': 2567}
44 {'RADSENS_1': 2563}
46 {'RADSENS_1': 2566}
48 {'RADSENS_1': 2569}
50 {'RADSENS_1': 2567}
52 {'RADSENS_1': 2570}
54 {'RADSENS_1': 2566}
56 {'RADSENS_1': 2568}
58 {'RADSENS_1': 2569}
60 {'RADSENS_1': 2569}
62 {'RADSENS_1': 2574}
33 {'RADSENS_1': 2922}
35 {'RADSENS_1': 2921}
37 {'RADSENS_1': 2921}
39 {'RADSENS_1': 2921}
41 {'RADSENS_1': 2922}
43 {'RADSENS_1': 2921}
45 {'RADSENS_1': 2921}
47 {'RADSENS_1': 2922}
49 {'RADSENS_1': 2921}
51 {'RADSENS_1': 2921}
53 {'RADSENS_1': 2921}
55 {'RADSENS_1': 2921}
57 {'RADSENS_1': 2921}
59 {'RADSENS_1': 2922}
61 {'RADSENS_1': 2921}
63 {'RADSENS_1': 2922}
32 {'RADSENS_2': 2549}
34 {'RADSENS_2': 2548}
36 {'RADSENS_2': 2550}
38 {'RADSENS_2': 2554}
40 {'RADSENS_2': 2554}
42 {'RADSENS_2': 2548}
44 {'RADSENS_2': 2550}
46 {'RADSENS_2': 2552}
48 {'RADSENS_2': 2552}
50 {'RADSENS_2': 2557}
52 {'RADSENS_2': 2550}
54 {'RADSENS_2': 2546}
56 {'RADSENS_2': 2553}
58 {'RADSENS_2': 2554}
60 {'RADSENS_2': 2549}
62 {'RADSENS_2': 2551}
33 {'RADSENS_2': 2905}
35 {'RADSENS_2': 2906}
37 {'RADSENS_2': 2905}
39 {'RADSENS_2': 2906}
41 {'RADSENS_2': 2905}
43 {'RADSENS_2': 2905}
45 {'RADSENS_2': 2906}
47 {'RADSENS_2': 2906}
49 {'RADSENS_2': 2906}
51 {'RADSENS_2': 2906}
53 {'RADSENS_2': 2905}
55 {'RADSENS_2': 2906}
57 {'RADSENS_2': 2905}
59 {'RADSENS_2': 2906}
61 {'RADSENS_2': 2906}
63 {'RADSENS_2': 2904}
32 {'RADSENS_3': 2535}
34 {'RADSENS_3': 2533}
36 {'RADSENS_3': 2535}
38 {'RADSENS_3': 2534}
40 {'RADSENS_3': 2529}
42 {'RADSENS_3': 2529}
44 {'RADSENS_3': 2533}
46 {'RADSENS_3': 2535}
48 {'RADSENS_3': 2531}
50 {'RADSENS_3': 2531}
52 {'RADSENS_3': 2534}
54 {'RADSENS_3': 2538}
56 {'RADSENS_3': 2531}
58 {'RADSENS_3': 2534}
60 {'RADSENS_3': 2536}
62 {'RADSENS_3': 2534}
33 {'RADSENS_3': 2889}
35 {'RADSENS_3': 2889}
37 {'RADSENS_3': 2889}
39 {'RADSENS_3': 2890}
41 {'RADSENS_3': 2889}
43 {'RADSENS_3': 2890}
45 {'RADSENS_3': 2889}
47 {'RADSENS_3': 2890}
49 {'RADSENS_3': 2890}
51 {'RADSENS_3': 2889}
53 {'RADSENS_3': 2889}
55 {'RADSENS_3': 2888}
57 {'RADSENS_3': 2890}
59 {'RADSENS_3': 2888}
61 {'RADSENS_3': 2889}
63 {'RADSENS_3': 2889}
32 {'RADSENS_4': 2526}
34 {'RADSENS_4': 2527}
36 {'RADSENS_4': 2533}
38 {'RADSENS_4': 2531}
40 {'RADSENS_4': 2525}
42 {'RADSENS_4': 2527}
44 {'RADSENS_4': 2524}
46 {'RADSENS_4': 2530}
48 {'RADSENS_4': 2528}
50 {'RADSENS_4': 2527}
52 {'RADSENS_4': 2529}
54 {'RADSENS_4': 2526}
56 {'RADSENS_4': 2525}
58 {'RADSENS_4': 2530}
60 {'RADSENS_4': 2527}
62 {'RADSENS_4': 2525}
33 {'RADSENS_4': 2884}
35 {'RADSENS_4': 2884}
37 {'RADSENS_4': 2883}
39 {'RADSENS_4': 2882}
41 {'RADSENS_4': 2884}
43 {'RADSENS_4': 2884}
45 {'RADSENS_4': 2883}
47 {'RADSENS_4': 2884}
49 {'RADSENS_4': 2883}
51 {'RADSENS_4': 2883}
53 {'RADSENS_4': 2883}
55 {'RADSENS_4': 2883}
57 {'RADSENS_4': 2884}
59 {'RADSENS_4': 2883}
61 {'RADSENS_4': 2884}
63 {'RADSENS_4': 2885}
