Timestamp GlobalTime(s) Dose(Mrad)
Start Temperature Measurements
REGCONF , SENSOR , ADC Value

32 {'TEMPSENS_1': 961}
34 {'TEMPSENS_1': 958}
36 {'TEMPSENS_1': 954}
38 {'TEMPSENS_1': 959}
40 {'TEMPSENS_1': 956}
42 {'TEMPSENS_1': 956}
44 {'TEMPSENS_1': 958}
46 {'TEMPSENS_1': 953}
48 {'TEMPSENS_1': 953}
50 {'TEMPSENS_1': 951}
52 {'TEMPSENS_1': 960}
54 {'TEMPSENS_1': 950}
56 {'TEMPSENS_1': 955}
58 {'TEMPSENS_1': 949}
60 {'TEMPSENS_1': 954}
62 {'TEMPSENS_1': 958}
33 {'TEMPSENS_1': 1395}
35 {'TEMPSENS_1': 1395}
37 {'TEMPSENS_1': 1396}
39 {'TEMPSENS_1': 1395}
41 {'TEMPSENS_1': 1395}
43 {'TEMPSENS_1': 1395}
45 {'TEMPSENS_1': 1395}
47 {'TEMPSENS_1': 1395}
49 {'TEMPSENS_1': 1396}
51 {'TEMPSENS_1': 1395}
53 {'TEMPSENS_1': 1396}
55 {'TEMPSENS_1': 1396}
57 {'TEMPSENS_1': 1395}
59 {'TEMPSENS_1': 1396}
61 {'TEMPSENS_1': 1396}
63 {'TEMPSENS_1': 1395}
32 {'TEMPSENS_2': 943}
34 {'TEMPSENS_2': 936}
36 {'TEMPSENS_2': 934}
38 {'TEMPSENS_2': 939}
40 {'TEMPSENS_2': 931}
42 {'TEMPSENS_2': 938}
44 {'TEMPSENS_2': 934}
46 {'TEMPSENS_2': 938}
48 {'TEMPSENS_2': 932}
50 {'TEMPSENS_2': 936}
52 {'TEMPSENS_2': 937}
54 {'TEMPSENS_2': 940}
56 {'TEMPSENS_2': 933}
58 {'TEMPSENS_2': 938}
60 {'TEMPSENS_2': 935}
62 {'TEMPSENS_2': 937}
33 {'TEMPSENS_2': 1377}
35 {'TEMPSENS_2': 1379}
37 {'TEMPSENS_2': 1377}
39 {'TEMPSENS_2': 1377}
41 {'TEMPSENS_2': 1379}
43 {'TEMPSENS_2': 1377}
45 {'TEMPSENS_2': 1379}
47 {'TEMPSENS_2': 1377}
49 {'TEMPSENS_2': 1377}
51 {'TEMPSENS_2': 1378}
53 {'TEMPSENS_2': 1378}
55 {'TEMPSENS_2': 1377}
57 {'TEMPSENS_2': 1378}
59 {'TEMPSENS_2': 1378}
61 {'TEMPSENS_2': 1378}
63 {'TEMPSENS_2': 1378}
32 {'TEMPSENS_3': 918}
34 {'TEMPSENS_3': 917}
36 {'TEMPSENS_3': 915}
38 {'TEMPSENS_3': 916}
40 {'TEMPSENS_3': 917}
42 {'TEMPSENS_3': 914}
44 {'TEMPSENS_3': 913}
46 {'TEMPSENS_3': 914}
48 {'TEMPSENS_3': 915}
50 {'TEMPSENS_3': 919}
52 {'TEMPSENS_3': 921}
54 {'TEMPSENS_3': 917}
56 {'TEMPSENS_3': 915}
58 {'TEMPSENS_3': 915}
60 {'TEMPSENS_3': 915}
62 {'TEMPSENS_3': 916}
33 {'TEMPSENS_3': 1360}
35 {'TEMPSENS_3': 1360}
37 {'TEMPSENS_3': 1360}
39 {'TEMPSENS_3': 1360}
41 {'TEMPSENS_3': 1359}
43 {'TEMPSENS_3': 1359}
45 {'TEMPSENS_3': 1360}
47 {'TEMPSENS_3': 1360}
49 {'TEMPSENS_3': 1360}
51 {'TEMPSENS_3': 1360}
53 {'TEMPSENS_3': 1359}
55 {'TEMPSENS_3': 1359}
57 {'TEMPSENS_3': 1360}
59 {'TEMPSENS_3': 1359}
61 {'TEMPSENS_3': 1360}
63 {'TEMPSENS_3': 1359}
32 {'TEMPSENS_4': 917}
34 {'TEMPSENS_4': 913}
36 {'TEMPSENS_4': 919}
38 {'TEMPSENS_4': 918}
40 {'TEMPSENS_4': 920}
42 {'TEMPSENS_4': 920}
44 {'TEMPSENS_4': 914}
46 {'TEMPSENS_4': 918}
48 {'TEMPSENS_4': 915}
50 {'TEMPSENS_4': 918}
52 {'TEMPSENS_4': 912}
54 {'TEMPSENS_4': 914}
56 {'TEMPSENS_4': 918}
58 {'TEMPSENS_4': 917}
60 {'TEMPSENS_4': 917}
62 {'TEMPSENS_4': 915}
33 {'TEMPSENS_4': 1360}
35 {'TEMPSENS_4': 1360}
37 {'TEMPSENS_4': 1359}
39 {'TEMPSENS_4': 1360}
41 {'TEMPSENS_4': 1359}
43 {'TEMPSENS_4': 1359}
45 {'TEMPSENS_4': 1360}
47 {'TEMPSENS_4': 1360}
49 {'TEMPSENS_4': 1361}
51 {'TEMPSENS_4': 1359}
53 {'TEMPSENS_4': 1360}
55 {'TEMPSENS_4': 1360}
57 {'TEMPSENS_4': 1359}
59 {'TEMPSENS_4': 1359}
61 {'TEMPSENS_4': 1360}
63 {'TEMPSENS_4': 1360}
32 {'RADSENS_1': 2564}
34 {'RADSENS_1': 2573}
36 {'RADSENS_1': 2570}
38 {'RADSENS_1': 2569}
40 {'RADSENS_1': 2568}
42 {'RADSENS_1': 2567}
44 {'RADSENS_1': 2563}
46 {'RADSENS_1': 2565}
48 {'RADSENS_1': 2567}
50 {'RADSENS_1': 2566}
52 {'RADSENS_1': 2569}
54 {'RADSENS_1': 2566}
56 {'RADSENS_1': 2568}
58 {'RADSENS_1': 2569}
60 {'RADSENS_1': 2569}
62 {'RADSENS_1': 2574}
33 {'RADSENS_1': 2922}
35 {'RADSENS_1': 2921}
37 {'RADSENS_1': 2921}
39 {'RADSENS_1': 2921}
41 {'RADSENS_1': 2921}
43 {'RADSENS_1': 2922}
45 {'RADSENS_1': 2922}
47 {'RADSENS_1': 2922}
49 {'RADSENS_1': 2922}
51 {'RADSENS_1': 2922}
53 {'RADSENS_1': 2921}
55 {'RADSENS_1': 2921}
57 {'RADSENS_1': 2922}
59 {'RADSENS_1': 2921}
61 {'RADSENS_1': 2922}
63 {'RADSENS_1': 2921}
32 {'RADSENS_2': 2546}
34 {'RADSENS_2': 2549}
36 {'RADSENS_2': 2550}
38 {'RADSENS_2': 2553}
40 {'RADSENS_2': 2555}
42 {'RADSENS_2': 2549}
44 {'RADSENS_2': 2551}
46 {'RADSENS_2': 2553}
48 {'RADSENS_2': 2552}
50 {'RADSENS_2': 2556}
52 {'RADSENS_2': 2551}
54 {'RADSENS_2': 2547}
56 {'RADSENS_2': 2552}
58 {'RADSENS_2': 2553}
60 {'RADSENS_2': 2548}
62 {'RADSENS_2': 2552}
33 {'RADSENS_2': 2905}
35 {'RADSENS_2': 2906}
37 {'RADSENS_2': 2906}
39 {'RADSENS_2': 2905}
41 {'RADSENS_2': 2905}
43 {'RADSENS_2': 2905}
45 {'RADSENS_2': 2906}
47 {'RADSENS_2': 2905}
49 {'RADSENS_2': 2905}
51 {'RADSENS_2': 2905}
53 {'RADSENS_2': 2905}
55 {'RADSENS_2': 2905}
57 {'RADSENS_2': 2905}
59 {'RADSENS_2': 2906}
61 {'RADSENS_2': 2905}
63 {'RADSENS_2': 2905}
32 {'RADSENS_3': 2533}
34 {'RADSENS_3': 2532}
36 {'RADSENS_3': 2534}
38 {'RADSENS_3': 2534}
40 {'RADSENS_3': 2531}
42 {'RADSENS_3': 2529}
44 {'RADSENS_3': 2532}
46 {'RADSENS_3': 2534}
48 {'RADSENS_3': 2531}
50 {'RADSENS_3': 2530}
52 {'RADSENS_3': 2534}
54 {'RADSENS_3': 2536}
56 {'RADSENS_3': 2530}
58 {'RADSENS_3': 2533}
60 {'RADSENS_3': 2536}
62 {'RADSENS_3': 2533}
33 {'RADSENS_3': 2889}
35 {'RADSENS_3': 2888}
37 {'RADSENS_3': 2888}
39 {'RADSENS_3': 2889}
41 {'RADSENS_3': 2889}
43 {'RADSENS_3': 2889}
45 {'RADSENS_3': 2889}
47 {'RADSENS_3': 2888}
49 {'RADSENS_3': 2889}
51 {'RADSENS_3': 2891}
53 {'RADSENS_3': 2888}
55 {'RADSENS_3': 2889}
57 {'RADSENS_3': 2889}
59 {'RADSENS_3': 2889}
61 {'RADSENS_3': 2890}
63 {'RADSENS_3': 2890}
32 {'RADSENS_4': 2527}
34 {'RADSENS_4': 2529}
36 {'RADSENS_4': 2533}
38 {'RADSENS_4': 2530}
40 {'RADSENS_4': 2525}
42 {'RADSENS_4': 2526}
44 {'RADSENS_4': 2525}
46 {'RADSENS_4': 2529}
48 {'RADSENS_4': 2529}
50 {'RADSENS_4': 2527}
52 {'RADSENS_4': 2528}
54 {'RADSENS_4': 2527}
56 {'RADSENS_4': 2525}
58 {'RADSENS_4': 2531}
60 {'RADSENS_4': 2528}
62 {'RADSENS_4': 2527}
33 {'RADSENS_4': 2884}
35 {'RADSENS_4': 2883}
37 {'RADSENS_4': 2885}
39 {'RADSENS_4': 2884}
41 {'RADSENS_4': 2882}
43 {'RADSENS_4': 2885}
45 {'RADSENS_4': 2883}
47 {'RADSENS_4': 2883}
49 {'RADSENS_4': 2883}
51 {'RADSENS_4': 2885}
53 {'RADSENS_4': 2885}
55 {'RADSENS_4': 2884}
57 {'RADSENS_4': 2884}
59 {'RADSENS_4': 2883}
61 {'RADSENS_4': 2883}
63 {'RADSENS_4': 2884}
