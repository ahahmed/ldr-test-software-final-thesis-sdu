from bdaq53.rd53a import RD53A
from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.fifo_readout import FifoReadout
from Ttest import Temperature_readout
import matplotlib.dates as mdate
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.dates import YearLocator, MonthLocator,HourLocator, DayLocator , DateFormatter
import matplotlib.pyplot as plt

#from bdaq53.serial_com import serial_com
from basil.utils.sim.utils import cocotb_compile_clean
from bdaq53.rd53a import RD53A
from bdaq53.analysis import analysis_utils
import pytemperature
import time
import os
import math
import datetime
import sys
import calendar
import numpy as np
import logging
import yaml
import time
import re
import csv
from matplotlib import pyplot as plt
from LdrTemp import meas_temperature
from LdrTemp import DeltaV_temperature

#Global 



#############################################
# Calibrate data
#
#############################################


# Rad NTC
Ntc = meas_temperature()

raw_input("enter")


TempLDR_instance = Temperature_readout() # create an instance of class Temperature_readout created by Mohsine 
sensors = DeltaV_temperature()
sensors.insert(0, time.time()) 



directory = '00_TEST_INFO'	
if not os.path.exists(directory):
   		os.makedirs(directory)	
timestamp = time.strftime("%d_%m_%Y_%H_%M_%S")
directory = '00_TEST_INFO/temperature_sensor/processed_data'		
if not os.path.exists(directory):
   		os.makedirs(directory)
#out_T_F = open(directory + '/' + "_temperature_sensor-test.txt")
out_T_F_W = open(directory +'/' + "_temperature_sensor.txt", "a")
out_T_F_W.write(', '.join(map(repr, sensors)) +	'\n')
out_T_F_W.close()





directory = '00_TEST_INFO/temperature_sensor/processed_data'


# Read RD53a temperature from File  (Delta-code data)
out_T_F_R = (directory +'/' + "_temperature_sensor.txt")
time      = []
tempsens1 = []
tempsens2 = []
tempsens3 = []
tempsens4 = []
radsens1  = []
radsens2  = []
radsens3  = []
radsens4  = []
#out_pdf = (directory +'/' + "_temperature_sensor.pdf")
with open(out_T_F_R, 'r') as file_in:
	reader = csv.reader(file_in, delimiter=',')
	for row in reader:
		time.append(row[0])
		tempsens1.append(float(row[1])) 
		tempsens2.append(float(row[2]))
		tempsens3.append(float(row[3])) 
		tempsens4.append(float(row[4])) 
		radsens1.append(float(row[5]))
		radsens2.append(float(row[6]))
		radsens3.append(float(row[7]))
		radsens4.append(float(row[8])) 
time = map(float, time)

tempsens1 = map(float, tempsens1)
tempsens2 = map(float, tempsens2)
tempsens3 = map(float, tempsens3)
tempsens4 = map(float, tempsens4)
radsens1 = map(float, radsens1)
radsens2 = map(float, radsens2)
radsens3 = map(float, radsens3)
radsens4 = map(float, radsens4)




#print time, tempsens1, tempsens2

Datetime = []
raw_input("enter")
for val in time:
	Datetime.append(mdate.epoch2num(val))


# Read NTC


k = 1.38064852*math.pow(10,-23)
q = 1.60217662*math.pow(10,-19)
V_refADC_SCC203 = 0.911 #[V]  
LSB = V_refADC_SCC203/math.pow(2, 12)
R = np.log(15)
Ntc_absolut = pytemperature.c2k(Ntc)


#print Ntc_absolut, LSB,tempsens1[-1]
raw_input("enter")
# Calculating Delta-V
tempsens1_Delta_V = tempsens1[-1]*LSB
tempsens2_Delta_V = tempsens2[-1]*LSB
tempsens3_Delta_V = float(tempsens3[-1])*float(LSB)
tempsens4_Delta_V = float(tempsens4[-1])*float(LSB)
Radsens1_Delta_V  = float(radsens1[-1])*float(LSB)
Radsens2_Delta_V  = float(radsens2[-1])*float(LSB)
Radsens3_Delta_V  = float(radsens3[-1])*float(LSB)
Radsens4_Delta_V  = float(radsens4[-1])*float(LSB)


raw_input("enter")

tempsens1_Nf = (tempsens1_Delta_V)/(((k*Ntc_absolut)/(q))*R)
tempsens2_Nf = (tempsens2_Delta_V)/(((k*Ntc_absolut)/(q))*R)
tempsens3_Nf = (tempsens3_Delta_V)/(((k*Ntc_absolut)/(q))*R)
tempsens4_Nf =  (tempsens4_Delta_V)/(((k*Ntc_absolut)/(q))*R) 
Radsens1_Nf  =  (Radsens1_Delta_V)/(((k*Ntc_absolut)/(q))*R)
Radsens2_Nf  =  (Radsens2_Delta_V)/(((k*Ntc_absolut)/(q))*R)
Radsens3_Nf  =  (Radsens3_Delta_V)/(((k*Ntc_absolut)/(q))*R)
Radsens4_Nf  =  (Radsens4_Delta_V)/(((k*Ntc_absolut)/(q))*R)

directory = '00_TEST_INFO/temperature_sensor/Calibration_Nf_f'		
if not os.path.exists(directory):
   		os.makedirs(directory)
#out_T_F = open(directory + '/' + "_temperature_sensor-test.txt")
out_T_F_W = open(directory +'/' + "_Nf-data.txt", "w")
#out_T_F_W = open(directory +'/' + "_Nf-data.txt", "w")
out_T_F_W.write(str(tempsens1_Nf)+ ",")
out_T_F_W.write(str(tempsens2_Nf )+ ",")
out_T_F_W.write(str(tempsens3_Nf )+ ",")
out_T_F_W.write(str(tempsens4_Nf )+ ",")
out_T_F_W.write(str(Radsens1_Nf )+ ",")
out_T_F_W.write(str(Radsens2_Nf )+ ",")
out_T_F_W.write(str(Radsens3_Nf )+ ",")
out_T_F_W.write(str(Radsens4_Nf ))

out_T_F_W.close()



#Calculation N_f for each 

T1 = 4285.2/tempsens1_Nf
T2 = 4285.2/tempsens2_Nf
T3 = 4285.2/tempsens3_Nf
T4 = 4285.2/tempsens4_Nf
R1 = 4285.2/Radsens1_Nf
R2 = 4285.2/Radsens2_Nf
R3 = 4285.2/Radsens3_Nf
R4 = 4285.2/Radsens4_Nf


# tempsens1.append(row[1]) 
# tempsens2.append(row[2]) 
# tempsens3.append(row[3]) 
# tempsens4.append(row[4]) 
# radsens1.append(row[5])
# radsens2.append(row[6])
# radsens3.append(row[7])
# radsens4.append(row[8]) 

tempsens1 = (pytemperature.k2c(T1*LSB*np.array(tempsens1)))
tempsens2 = (pytemperature.k2c(T2*LSB*np.array(tempsens2)))
tempsens3 = (pytemperature.k2c(T3*LSB*np.array(tempsens3)))
tempsens4 = (pytemperature.k2c(T4*LSB*np.array(tempsens4)))
radsens1 = (pytemperature.k2c(R1*LSB*np.array(radsens1)))
radsens2 = (pytemperature.k2c(R2*LSB*np.array(radsens2)))
radsens3 = (pytemperature.k2c(R3*LSB*np.array(radsens3)))
radsens4 = (pytemperature.k2c(R4*LSB*np.array(radsens4)))

# plotting 
# Create some mock data

fig = plt.figure()
ax1 = fig.add_subplot(111)

Hour = HourLocator()   # every year
day = DayLocator()  # every month


directory = '00_TEST_INFO/temperature_sensor/processed_data'		
if not os.path.exists(directory):
   		os.makedirs(directory)
#out_T_F = open(directory + '/' + "_temperature_sensor-test.txt")
out_T_F_W = open(directory +'/' + "temperaturein_C.txt", "a")
#out_T_F_W = open(directory +'/' + "_Nf-data.txt", "w")
out_T_F_W.write(str(tempsens1)+ ",")
out_T_F_W.write(str(tempsens2 )+ ",")
out_T_F_W.write(str(tempsens3 )+ ",")
out_T_F_W.write(str(tempsens4 )+ ",")
out_T_F_W.write(str(radsens1 )+ ",")
out_T_F_W.write(str(radsens2 )+ ",")
out_T_F_W.write(str(radsens3 )+ ",")
out_T_F_W.write(str(radsens4 ))

ax1.plot_date(Datetime, tempsens1,'r-')
ax1.plot_date(Datetime, tempsens2,'bs-') 
ax1.plot_date(Datetime, tempsens3,'g-') 
ax1.plot_date(Datetime, tempsens4,'b--') 

ax1.plot_date(Datetime, radsens1,'k-')
ax1.plot_date(Datetime, radsens2,'o-') 
ax1.plot_date(Datetime, tempsens3,'m-') 
ax1.plot_date(Datetime, tempsens4,'g--') 

# xtick format string
date_fmt = '%d-%m-%y %H'
# Use a DateFormatter to set the data to the correct format.
date_formatter = mdate.DateFormatter(date_fmt)
ax1.xaxis.set_major_formatter(date_formatter)
# tick labels diagonal so they fit easier.
fig.autofmt_xdate()
ax1.set_ylim((20,40))
ax1.xaxis.set_major_locator(day)
ax1.xaxis.set_minor_locator(Hour)
ax1.autoscale_view()
#ax1.gridon()


plt.grid(b=None, which='minor', axis='both')

plt.show()
# # pdf = PdfPages(out_pdf)

# # pdf.savefig(fig)

# # pdf.close()
